﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace TenioDLKiller
{
    public partial class AppForm : Form
    {
        Thread appWorkThrd;
        public AppForm()
        {
            InitializeComponent();
        }
        private void killerThread()
        {
            while (true)
            {
                Thread.Sleep(1000);
                Process kiCmd = new Process();
                //kiCmd.StartInfo.FileName = "taskkill";
                //kiCmd.StartInfo.Arguments = "/f /im teniodl.exe";
                //kiCmd.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //kiCmd.Start();
                //kiCmd.WaitForExit();
                Process[] killprocess = Process.GetProcessesByName("teniodl");
                foreach (Process p in killprocess) p.Kill();
            }
        }
        private void AppForm_Load(object sender, EventArgs e)
        {
            Hide();
            appWorkThrd = new Thread(new ThreadStart(killerThread));
            appWorkThrd.Start();
            Close();
        }
    }
}
